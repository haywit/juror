Juror
=====

An application which polls the NSW Juror website to determine whether you have been summoned to or excused from jury duty in NSW, Australia. This application is designed to be scheduled (eg. in a `systemd` job or with `launchd`) to run daily and notify you that you have been summoned long before the letter arrives in the mail.

Getting Started
===============

The easiest way to get started using juror is to follow these steps, but you can use any webhook instead of IFTTT and any scheduler instead of `launchd`.

Get an If This Then That account
--------------------------------

Sign up to IFTTT at [https://ifttt.com/](https://ifttt.com/) for free.

Create an applet with steps:

1. If "receive web request", with event name `juror`. [Use the "webhooks" service.](https://ifttt.com/maker_webhooks/)
2. Then "send a notification from the IFTTT app", with message `{{Value1}}`. [Use the "notifications" service.](https://ifttt.com/if_notifications/)
3. Find out your webhook URL [by going to the settings for the webhooks service](https://ifttt.com/maker_webhooks/settings) and noting down the URL. It should look like https://maker.ifttt.com/use/{key}.
4. Go to the URL you found in step 3 and note down the triggering URL. It should look like https://maker.ifttt.com/trigger/{event}/with/key/{key}. Substitute `{event}` for the trigger event name you used in step 1 (`juror` in this example).
5. Install the IFTTT app on your phone and log in to your account.

Create a credentials file
-------------------------

Create a file in the root of this repo called `credentials.json` which has the form:
```json
[
    {
        "number":"12345678",
        "dob":"01/01/1970",
        "webhook": "https://maker.ifttt.com/trigger/{event}/with/key/{key}?value1="
    }
]
```

Note that `dob` is your date of birth in format `dd/mm/yyyy` and `number` is your juror number from your notification that you are on the jury roll (this will arrive by mail if you are put on the roll). The webhook will have the message appended to the URL as a URL parameter, so don't forget to include the parameter name (eg. `?value1=` in this example).

You can add multiple juror credentials in the top level array if you want to check for your friends too.

Smoke test Juror
----------------

Run the application for the first time.

```bash
> cd my_juror_checkout
> python3 -m venv .venv
> . .venv/bin/activate
> pip install -r requirements.txt
...
> python -m juror
==> at https://www.juror.nsw.gov.au/home
Note: submit is using submit button: name='None', value='None'
==> at https://www.juror.nsw.gov.au/juror/details
==> at https://www.juror.nsw.gov.au/home
Dec 05 2020: We sent you a notice that you are on a jury roll.
Apr 10 2021: We sent you a summons to attend for jury service.
Apr 29 2021: You applied to be excused.
May 11 2021: We accepted your application to be excused.
You have been excused from jury duty
Sending notification 'You have been excused from jury duty. The latest event is from May 11 2021: We accepted your application to be excused.'
==> at https://maker.ifttt.com/trigger/juror/with/key/{key}?value1=You%20have%20been%20excused%20from%20jury%20duty.%20The%20latest%20event%20is%20from%20May%2011%202021:%20We%20accepted%20your%20application%20to%20be%20excused.
>
```

Add to `launchd`
----------------

This is for MacOS only. For other platforms you will need to pick a different scheduler (eg. `systemd`).

__Note:__ the repo must not be in `Documents` because scripts launched through `launchd` are sandboxed from `Documents` in modern OSX. Instead, put it somewhere like `~/Juror`.

Get the example `plist` file `scripts/com.haywit.juror.plist` and replace references to `{Repo Path}` with the actual path to the repo on your machine. Copy this file to `~/Library/LaunchAgents/`. Then run `launchctl load ~/Library/LaunchAgents/com.haywit.juror.plist`.

Done! You should now get a notification every day at 4:30pm telling you whether you have most recently been excused or summoned. If your computer is asleep at 4:30pm it will run the job the next time it wakes - with Power Nap this is usually within about an hour and a half.

