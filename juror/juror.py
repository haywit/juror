from twill import browser, commands
import bs4
import json

class Event():
    def __init__(self, date, text):
        self.date = date
        self.text = text

    def get_state(self):
        if "sent you a summons" in self.text:
            return "summoned"
        if "accepted your application to be excused" in self.text:
            return "excused"
        return None
    
    def __str__(self):
        return "{}: {}".format(self.date, self.text)


def pretty_state(state):
    if state == "excused":
        return "You have been excused from jury duty"
    elif state == "summoned":
        return "You have been summoned to jury duty"
    else:
        return "It looks like you have never been summoned to jury duty"


def get_credentials():
    with open("credentials.json", "r") as f:
        all_credentials = json.load(f)
    for credential in all_credentials:
        yield credential


def get_state(login):
    browser.go('https://www.juror.nsw.gov.au/home')
    commands.fv("jurorLoginForm", "jurorNumber", login["number"])
    commands.fv("jurorLoginForm", "dob", login["dob"])
    browser.submit('0')
    browser.go('https://www.juror.nsw.gov.au/juror/details')

    soup = bs4.BeautifulSoup(browser.html, 'html.parser')
    results = soup.find('div', class_='correspondence')
    correspondences = results.find_all('div', recursive=False)
    browser.go('https://www.juror.nsw.gov.au/logout')

    events = []
    for c in correspondences:
        date = c.find('h2')
        contents = c.find('p')
        events.append(Event(date.text.strip().rstrip(":"), contents.text.strip()))

    state = None
    for e in reversed(events):
        print(e)
        if e.get_state():
            state = e.get_state()

    print(pretty_state(state))
    return state, events


def send_message(webhook, message):
    print("Sending notification '{}'".format(message))
    browser.go("{}{}".format(webhook, message))


def main():
    for credential in get_credentials():
        state, events = get_state(credential)
        send_message(
            credential["webhook"],
            "{}. The latest event is from {}".format(
                pretty_state(state),
                events[0]
            ))
